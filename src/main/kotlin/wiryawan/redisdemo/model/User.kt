package wiryawan.redisdemo.model

import java.io.Serializable

data class User(

        var uuid:String = "",

        var name:String = "",

        var salary: Long
): Serializable
package wiryawan.redisdemo.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import wiryawan.redisdemo.model.User
import wiryawan.redisdemo.service.UserService
import kotlin.collections.HashMap

@RestController
class UserController(val userService: UserService) {

    @RequestMapping(value = ["/users"], method = [RequestMethod.GET])
    fun getAllUsers(): ResponseEntity<HashMap<String, Any>> {
        val data = userService.getAllUser()
        val response = HashMap<String, Any>()
        if (data == null) {
            response["message"] = "Data kosong"
            return ResponseEntity(response, HttpStatus.NO_CONTENT)
        }
        response["data"] = data
        return ResponseEntity.ok(response)
    }

    @RequestMapping(value = ["/user/{id}"], method = [RequestMethod.GET])
    fun getUserById(@PathVariable("id") id: String): ResponseEntity<HashMap<String, Any>> {
        val data = userService.findById(id)
        val response = HashMap<String, Any>()
        if (data == null) {
            response["message"] = "Data dengan " + data.uuid + " tidak ditemukan"
            return ResponseEntity(response, HttpStatus.NO_CONTENT)
        }
        response["data"] = data
        return ResponseEntity.ok(response)
    }

    @RequestMapping(value = ["/user"], method = [RequestMethod.POST])
    fun createUser(@RequestBody user: User): ResponseEntity<HashMap<String, Any>> {
        userService.createUser(user)
        val response = HashMap<String, Any>()
        response["message"] = "Data berhasil ditambahkan"
        return ResponseEntity.ok(response)
    }

    @RequestMapping(value = ["/user"], method = [RequestMethod.PUT])
    fun updateUser(@RequestBody user: User): ResponseEntity<HashMap<String, Any>> {
        userService.updateUser(user)
        val response = HashMap<String, Any>()
        response["message"] = "Data berhasil diperbarui"
        return ResponseEntity.ok(response)
    }


    @RequestMapping(value = ["/user/{id}"], method = [RequestMethod.DELETE])
    fun deleteUser(@PathVariable("id") id: String): ResponseEntity<HashMap<String, Any>> {
        userService.deleteUser(id)
        val response = HashMap<String, Any>()
        response["message"] = "Data berhasil dihapus"
        return ResponseEntity.ok(response)
    }

}
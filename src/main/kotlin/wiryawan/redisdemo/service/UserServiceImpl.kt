package wiryawan.redisdemo.service

import org.springframework.data.redis.core.HashOperations
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Service
import wiryawan.redisdemo.model.User
import java.util.*

@Service
class UserServiceImpl(redisTemplate: RedisTemplate<String, Any>): UserService {

    val hashOperations: HashOperations<String, String, Any> = redisTemplate.opsForHash<String, Any>()

    override fun getAllUser(): Map<String, Any> {
        return hashOperations.entries("USER")
    }

    override fun findById(id: String): User {
        return (hashOperations.get("USER", id) as User?)!!
    }

    override fun createUser(user: User) {
        val uuid = UUID.randomUUID()
        user.uuid = uuid.toString()
        hashOperations.put("USER", user.uuid, user)
    }

    override fun updateUser(user: User) {
        createUser(user)
    }

    override fun deleteUser(id: String) {
        hashOperations.delete("USER", id)
    }
}
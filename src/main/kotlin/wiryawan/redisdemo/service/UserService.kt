package wiryawan.redisdemo.service

import wiryawan.redisdemo.model.User

interface UserService {

    fun getAllUser(): Map<String, Any>

    fun findById(id: String): User

    fun createUser(user: User)

    fun updateUser(user: User)

    fun deleteUser(id: String)


}